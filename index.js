// 1. Given two variables num1 and num2, create a function that would swap the values of num1 and num2 without the use of a third variable. Hint: Make use of arithmetic operations as needed.
function switchNumber(num1, num2) {
    // Rule 1: Do not create a new variable.
    // Rule 2: Use an arithmetic operation.

    // Code area to swap num1 and num2.

    //num1 = 4
    //num2 = 3

    num1 = num1 + num2;  // 4 + 3 = 7 <new value of num1 = 7>
    num2 = num1 - num2;  // 7 - 3 = 4 <new value of num2 = 4>
    num1 = num1 - num2;  // 7 - 4 = 3 <new value of num1 = 3>


    // Do not change anything beyond this line. 

    return { num1, num2 };
}

// 2. Create a function that would count the number of times a letter occurs in a string. Other things to consider: the input to the function must be a letter and a string.
function countLetter(letter, sentence) {
    let result = 0;

    while(letter.length===1){

        for(c=0; c < sentence.length; c++){
            if(sentence.charAt(c) == letter){
                result ++;
            }
        }
        return result;
    }

}

/*answer key:

    if(letter.length > 1){

        return undefined

    } else {
        //iterate over every character of the sentence
        for(let i = 0; i < sentence.length; i++){
    
            //compare each character with the passed in letter
            if(sentence[i] === letter){
                
                return++;
            }

        }
        //for every match found, increment my result
         retrun result;
    }
*/

    // Using a loop, check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

//     const validLetter = 'o';
//     const invalidLetter = 'abc';
//     const sentence = 'The quick brown fox jumps over the lazy dog';

//     it('Invalid letter returns undefined.', function() {
//         const result = source.countLetter(invalidLetter, sentence);
//         expect(result).toBeUndefined();
//     });

//     it('Valid letter returns number of occurrence.', function() {
//         const result = source.countLetter(validLetter, sentence);
//         expect(result).toEqual(expect.any(Number));
//     });
// });


3. Given a string, determine if it is a palindrome or not. A palindrome is a string that is the same when read from left to right and right to left. Usage of Array.reverse() IS NOT allowed.

function isPalindrome(phrase) {
    // If a phrase is palindrome, return true. If not, return false.
    // The function should first remove all spaces within the phrase.
    // The function should also check for the letters regardless of its casing.


    // const palindrome = 'Was it a car or a cat I saw';
    // const notPalindrome = 'Hello world!';

    low = phrase.toLowerCase();
    regExp = low.replace(/[\W_]/g, "");
    s = regExp.split("");
    r = s.reverse();
    j = r.join("");
    if (regExp == j)
     {return true;}
   else {return false;}
    
}
isPalindrome("Was it a car or a cat I saw")
isPalindrome("Hello world!")

/* answer key

        //remove the whitespaces from my phrase
        //disregard case-sensitivity
    phrase = phrase.replace("", "").toLowerCase()
        //phrase = phrase.split('').join('').toLowerCase

    
        //if NOT the same, return false
        //if same, remove the outermost characters from my phrase and repeat
        //while there's more than 1 character in the phrase, keep on iterating/comparing
    while(phrase.length > 1){
        //compare the first a nd last characters of the phrase
        if(phrase[0] == phrase[phrase.length-1]){
            phrase = phrase.substr(1, phrase.length-2)
        }esle {
            return false;
        }
    }

    if(phrase.length == 1 || phrase.lenght == 0){
    
        return true;
    }
*/

4. Given a string, create a function that will output true if given string is an isogram and false otherwise. Disregard character case. An isogram is a word with no repeating characters (ie. "city" is an isogram, "hello" is not)

function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    const isogram = 'Machine';
    const notIsogram = 'Hello';

    
        if (typeof text !== "string" || text.length === 0) return false;

        text = text.trim().toLowerCase();

        for (let i = 0; i < text.length; i++) {
                if (text.lastIndexOf(text[i]) > i) return false;
        }
        return true;
}
isIsogram("Machine")
isIsogram("Hello")

/* answer key

        // convert the text characters to lower case
        // iterate over every character of the string
        // for each character iterated, chec for duplicate
    
    for(let charCounter = 0; charCounter < text.length; charCounter ++){
        
        for(let checkCounter = charCounter + 1; checkCounter < text.length; checkCounter ++){
            
            // if duplicate has been found, return false
            if(text[charCounter].toLowerCase() === text[checkCounter].toLowerCase()){
            
                return false
            }
        }
    }
    //if no duplicate found, text is an isogram - return true
    return true 


====> another answer key

    text = text.toLowerCase();
    let letters =[];

    for(let 1 = 0; i < text.length; i++){
        
        if(letters.indexOf(text[i] !== -1)){
        
            return false;
        }{
    
            letters.push(text[i]);
        }
    }
    return true;
*/

5. Students aged 13 to 21 years old as well as senior citizens (65 and up) both enjoy a 20% discount on purchased goods. Students below 13 years of age cannot purchase without their parent's consent. Given an age and a price, create a function that will output UNDEFINED if age is below permitted age or the final purchase price otherwise. Final purchase price must have TWO decimal places.

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.

    // const price = 109.4356;
    // const discountedPrice = price * 0.8;
    // const roundedPrice = discountedPrice.toFixed(2);


    let discount = price * 0.8

    if (age < 13){

        return undefined;

    }else if((age >= 13 && age <= 21 || age >= 65)){

        return discount.toFixed(2);

    } else {

        return price.toFixed(2)
    }
}



6. Create a function that will return an array of DISTINCT categories that have at least ONE of its items sold out. If all items are in stock, an empty array will be returned instead.

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // const items = [
    //     { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
    //     { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
    //     { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
    //     { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
    //     { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
    // ];

        var object = {};
        var result = [];

    items.forEach(function (item) {
          if(!object[item])
              object[item] = 0;
            object[item] += 1;
        })

        for (var prop in object) {
           if(object[prop] >= 2) {
               result.push(prop);
           }
        }

        return result;
}

/*  answer key

    let hotCategories = [];

    items.forEach(item => {
    
        if(item.stocks === 0){
            
            if(hotCategories.indexOf(item.category) === -1){
                hotcategories.push(item.category);
            }
        }
    })
    return hotCategories;
*/


7. Given 2 arrays of voter ID's representing the votes of two different candidates, create a function that will return an array of voter ID's who voted for BOTH candidates. If no flying voters identified, return an empty array instead.
function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].

    
}

/* answer key

    let findFlyingVoters = [];

    candidateA.forEach(voteA => {
    
        if(candidateB.indexOf(voteA) !== -1){
            
            flyingVoters.push(voteA);
        }    
    })
    
    return flyingVoters;
*/

8. Given an array of objects each representing an ad campaign of a particular brand, create a function that will return an array of objects with the brand name and their corresponding adEfficiency which is computed as (customers gained / expenditure) x 100. SORT this array in DESCENDING order based on adEfficiency.

function calculateAdEfficiency(adCampaigns) {
    // Sort the ad efficiency according to the most to least efficient.

    // The passed campaigns array from the test are the following:
    // { brand: 'Brand X', expenditure: 12345.89, customersGained: 4879 }
    // { brand: 'Brand Y', expenditure: 22456.17, customersGained: 6752 }
    // { brand: 'Brand Z', expenditure: 18745.36, customersGained: 5823 }

    // The efficiency is computed as (customersGained / expenditure) x 100.

    // The expected output after processing the campaigns array are:
    // { brand: 'Brand X', adEfficiency: 39.51922461645131 }
    // { brand: 'Brand Z', adEfficiency: 31.063687227132476 }
    // { brand: 'Brand Y', adEfficiency: 30.06746030155632 }

    
}

/* answer key

    const adEffeciencies = adCampaigns.map(campaign => {
    
        return {
            brand: campagin.brand,
            adEffeciency: (campaign.customersGained / campaign.expenditure) * 100
        }
    })

    adEffeciencies.sort((campaignA, campaignB)=>{
        
        return campaignB.adEffiency - campaignA.adEfficiency;
    })

    return adEfficiencies
*/

module.exports = {
    switchNumber,
    countLetter,
    isPalindrome,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters,
    calculateAdEfficiency
};